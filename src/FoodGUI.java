import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton ramenButton;
    private JButton tempuraButton;
    private JButton gyozaButton;
    private JButton karaageButton;
    private JButton checkOutButton;
    private JPanel totalPanel;
    private JTextPane order;
    private JTextPane maney;

    int total = 0;

    void order(String food) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+ "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Thank you for ordering "+food+"! It will be served as soon as possible.");
            order.setText(order.getText() +food+ "\n");
            total += 100;
            maney.setText(total + "yen");
        }
    }


      public FoodGUI(){
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            { order("Tempura");
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");

            }
        });
          checkOutButton.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                  int checkout = JOptionPane.showConfirmDialog(null,
                          "Would you like to checkout ?",
                          "checkout",
                          JOptionPane.YES_NO_OPTION);

                  if (checkout == 0) {
                      JOptionPane.showMessageDialog(null,
                              "Thank you. The total price is " +total+ " yen.");
                      order.setText("");
                      total = 0;
                      maney.setText(total + " yen");
              }}
          });
      }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
